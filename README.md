# Adventure Box

## stack:

front
* React
* TypeScript
* CSS Grid, Flexbox
* Web Components
* styled-components
* Redux
* axios
* eslint
* Jest + Enzyme

back
* NodeJS
* MongoDB, Mongose
* Express
